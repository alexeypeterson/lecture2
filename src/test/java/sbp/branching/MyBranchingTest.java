package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;


public class MyBranchingTest {

    /**
     * Проверка maxInt при utilFunc2 = true
     */
    @Test
    public void maxInt_True_Test() {
        int i1 = 1;
        int i2 = 7;
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);

        int result = myBranching.maxInt(i1,i2);
        Assertions.assertEquals(0,result);
        Mockito.verify(utilsMock, Mockito.times(7)).utilFunc1(anyString());
    }

    /**
     * Проверка maxInt при utilFunc2 = false
     */
    @Test
    public void maxInt_False_Test() {
        int i1 = 1;
        int i2 = 7;
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);

        int result = myBranching.maxInt(i1,i2);
        Assertions.assertEquals(7,result);
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());
    }

    /**
     * Проверка ifElseExample при utilFunc2 = true
     */
    @Test
    public void ifElseExample_True_Test() {

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);

        boolean result = myBranching.ifElseExample();
        Assertions.assertTrue(result);
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());
    }

    /**
     * Проверка ifElseExample при utilFunc2 = false
     */
    @Test
    public void ifElseExample_False_Test() {

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);

        boolean result = myBranching.ifElseExample();
        Assertions.assertFalse(result);
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());
    }

    /**
     * Проверка switchExample
     * Utils#utilFunc2 = true и i = 0
     *
     */
    @Test
    public void switchExample_True0_Test() {

        int i = 0;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());

    }

    /**
     * Проверка switchExample
     * Utils#utilFunc2 = false и i = 0
     *
     */
    @Test
    public void switchExample_False0_Test() {

        int i = 0;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());
    }

    /**
     * Проверка switchExample при i = 1
     *
     */
    @Test
    public void switchExample_1_Test() {

        int i = 1;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();

    }

    /**
     * Проверка switchExample при i = 2
     *
     */
    @Test
    public void switchExample_2_Test() {

        int i = 2;

        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);

        myBranching.switchExample(i);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(anyString());

    }






}


